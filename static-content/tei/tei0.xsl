<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Mar 29, 2011</xd:p>
            <xd:p><xd:b>Author:</xd:b> sdm7g</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    
    <xsl:template match="p">
        <p><xsl:apply-templates /></p>
    </xsl:template>
    
    <xsl:template match="lb">
        <br /><xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="list" >
        <ul><xsl:apply-templates /></ul>
    </xsl:template>
    
    <xsl:template match="item">
        <li><xsl:apply-templates /></li>
    </xsl:template>
    
</xsl:stylesheet>
