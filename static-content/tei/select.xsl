<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Mar 25, 2011</xd:p>
            <xd:p><xd:b>Author:</xd:b> sdm7g</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
	<!-- param values for testing -->
    <xsl:param name="division">div1</xsl:param>
    <xsl:param name="part">all</xsl:param>

    <xsl:include href="html/tei.xsl"/>

    <xsl:template match="/">
    <!--
        <html><head>
          <title><xsl:value-of select="$division" />[<xsl:value-of select="$part" />]</title></head>
            <body>  -->
        

		<xsl:choose>
			<xsl:when test="$part = 'all'" >
			    <xsl:apply-templates />
			</xsl:when>

			<xsl:when test="$part = 'teiHeader'" >
			    <xsl:apply-templates  select="//teiHeader" />
			</xsl:when>

			<xsl:when test="$part = 'front'" >
			    <xsl:apply-templates select="//front" />
			</xsl:when>

			<xsl:when test="$part = 'back'" >
			    <xsl:apply-templates select="//back" />
			</xsl:when>

			<xsl:when test="$part = 'body'" >
			    <xsl:apply-templates select="//body" />
			</xsl:when>
			
			<xsl:otherwise>
            <xsl:if test="number($part) and $division" >

                <xsl:choose>
                    <xsl:when test="$division = 'div1'">
                        <xsl:apply-templates select="(//body//div1)[number($part)]" />
                    </xsl:when>
                    <xsl:when test="$division = 'div2'">
                        <xsl:apply-templates select="(//body//div2)[number($part)]" />
                    </xsl:when>
                    <xsl:otherwise>NOTHING SELECTED</xsl:otherwise>
                </xsl:choose>                

            </xsl:if>
			
			</xsl:otherwise>

			
		</xsl:choose>
           
            
       <!--  </body></html> -->
    </xsl:template>

 
</xsl:stylesheet>
