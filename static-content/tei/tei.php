<?php
// xslt in php v.2 
// get xml, xsl from query params
// look inside xsl to determine output type 
// and set Content-Type: header

	$style = "select.xsl" ;
	$xml = htmlspecialchars($_GET["file"]);
	$division = htmlspecialchars($_GET["division"]);
	$part = htmlspecialchars($_GET["part"]);

	if ( $style ) {
		$xslDoc = new DOMDocument();
		$xslDoc->load( $style );
		
		$sxpath = new DOMXpath( $xslDoc );
		$xmethod = $sxpath->query( '//xsl:output/@method' );
		
		if ( $xmethod->length == 1 ) { 
		    $ctype = $xmethod->item(0)->nodeValue; 
		    if ( $ctype == "html" ) { $ctype = "text/html" ; }
		    else if ( $ctype =="text" ) { $ctype = "text/plain" ; }
		    else if ( $ctype == "xml" ) { $ctype = "text/xml" ; } 
		    else { $ctype = "text/html" ; } 
		} else { $ctype = "text/html" ; } 
	}

	header( 'Content-Type: ' . $ctype );

    if ( $ctype == "text/html" ) {	
?>
<html>

<head><title><?php echo $xml ?></title></head>
<body>

<h2><?php echo "LIBXSLT Version: " , LIBXSLT_DOTTED_VERSION ?></h2>

<ul>
	<li>file: <?php echo $xml ?></li>
	<li>style: <?php echo $style ?></li>
	<li>content-type: <?php echo $ctype ?></li>
	<?php if ( $part ) { echo "<li>part: " . $part . "</li>" ; } ?>
	<?php if ( $division ) { echo "<li>division: " . $division . "</li>" ; } ?>	
</ul>

<div class="content" >
<?php } ?>
<?php

	if ( $xml and $style ) {
		$xmlDoc = new DOMDocument();
		$xmlDoc->load( $xml );
		
		$proc = new XSLTProcessor();
		if ( $division ) { $proc->setParameter( "", "division", $division ); }
		if ( $part ) { $proc->setParameter( "", "part", $part ); }
		$proc->importStylesheet($xslDoc);
		echo $proc->transformToXML($xmlDoc);		
	}	
?>
<?php if ( $ctype == "text/html" ) { ?>
</div>

</body>
</html>
<?php  }  ?>