<HTML>
<head>
<TITLE>Daniel Kinney, "Some Philologies of the Future"</TITLE></head><body bgcolor="#FFFFF0" background="../thumbnails/paper.gif">

<P><CENTER><b><font size=+1>Some Philologies of the Future</font></b><br><a <href="http://www.people.virginia.edu/~jdk3t/">Daniel Kinney</a>, Department of English</CENTER>
</P>


<P>	You have been met with several alluring descriptions of this day's events<A href="#N_1_"><SUP><font size=-2>1</font></SUP></A>�as a "rite of spring," "a
ceremony of lost innocence," "a collective reverie," even "a dance"�but for some reason, nobody bothered
to add, "philological frolic." Festive prospects like those do not quite preempt talk of philology, but they do
make it timely to ask what she's doing trying to move in these circles.  What does bookkeeper she have to
do with release or abandon or new possibilities?  What does she have to do with the future?</P>

<P>	Philology has an air of old-fangled about her, like Prudence and Temperance and Chastity, all apt
to seem trapped by their names, which are also their job-descriptions; this is true even though she<EM> </EM>once had
a top-flight starring role in a blockbuster-book of her own, Martianus Capella's <EM>The Marriage of Philology and
Mercury</EM>, even winning a long-running dream-assignation with the globe-trotting god of all millionaires.  That was then; this is now, and 
the profile appears to be pinching a bit, if we trust the assessment of eminent classicist Bernard Knox ("On
Two Fronts," from his volume engagingly titled <EM>Backing into the Future</EM><A href="#N_2_"><SUP><font size=-2>2</font></SUP></A>):<br><table width=92% align=center><tr><td>A look at the contents of the most recent volume of <EM>Transactions and Proceedings of the
American Philological Association</EM> conjures up a vision of the Muse of Philology with a Virginia
Slims cigarette over the caption "You've come a long way, baby."</td></tr></table>Something tells me that Bernard Knox does not have a high opinion of Virginia Slims babies, but this image
further hints at a kind of failed makeover, a kind of decayed-gentlewoman scenario with no genuine options
of any sort.  Philology deserves better, and assuredly she has been chained all too long to this feminine-abstraction persona which she's poised to shed; a material girl, she, he, it, or indifferent is not bound to take type-casts from
anyone.</P>

<P>	What then <i>is</i> philology?  Borrowing from Jan Ziolkowski's  low-key summing-up of the subject,
from a slim conference-volume on the question:<A href="#N_3_"><SUP><font size=-2>3</font></SUP></A><br><table width=92% align=center><tr><td>As the Byzantinist Ihor �evcenko once observed ... "Philology is constituting and interpreting
the texts that have come down to us.  It is a narrow thing, but without it nothing else is possible." 
This definition accords with Saussure's succinct description of the mission of philology:
"especially to correct, interpret, and comment upon texts."</td></tr></table>This description merits praise for the way it fits what we now generally mean by philology for better as well
as for worse; you can hear in it some of what I heard from one interviewer from Berkeley some twenty years
since�"We don't normally interview people who're doing an edition-dissertation"�and can hear in it some
of Erasmus' well-borne-out insistence some years before that that "these 'trifles' lead on to important
things."<A href="#N_4_"><SUP><font size=-2>4</font></SUP></A>  Quite apart from the same old sad type-casts, there may indeed be a distinct philological mind-set 
that should get its own flicker of notice at our frolic-forecast today; but it is not only in this particular sense
that we need to contend with a future philology, since the word itself casts its own nets vastly wider than this,
and one way or another, names changed and ambitions restated, we all seem to be in the philology business,
at least for the time being and the knowable future, whether we like the notion or not.</P>

<P>	That asserts quite a lot; so what else is philology?  The word sets out with great expectations; it
means "love of <EM>logos</EM>," of language, of theory, of language <EM>par excellence</EM>, learning in general; Leo Braudy
can gloss it as "love of the word."  It can signify encyclopedic reach, thorough-going common-knowledge
reappraisal/review (Montaigne's pitting of text against text, evidence against evidence), or else learning for
learning's sake, learning for show, the vainglorious "science of signs" that Augustine once blasts as opposing
and thwarting a charitable "science of things."<A href="#N_5_"><SUP><font size=-2>5</font></SUP></A>  There is no human discourse not part of this broader
philology, and most of our favorite curricular rubrics could be taken as specialty-glosses on its main concerns,
from linguistics to media-studies to semiotics and cultural history, just as <EM>Studies in Philology</EM>'s latest
contents�re-vamped as they have been along Knox's Virginia-Slims lines�would seem equally at home in quite
different journals.<A href="#N_6_"><SUP><font size=-2>6</font></SUP></A>  Nonetheless, we are less apt to cast the philologist as a lover of cultural discourse writ large
than as a great devotee of good documentation, a description not only less damning than it probably sounds,
but moreover well short of mere caricature.  Though Greek <EM>logos</EM> is just as much "theory" as "language,"
the voracious inclusiveness of Renaissance philology makes it seem more a matter of gathering and
classification or a kind of perfunctory preamble to theory than a vigorous, methodical furtherer of  theory
per se; in fact humanists wage war on medieval theorizing as impeding their quest for particulars.    Thus
routinely aligned with description and not with  analysis, the philologist's efforts are too readily classed in
pejorative doublets as antiquarianism <EM>rather</EM> than interpretation and as source-hunting <EM>rather</EM> than criticism.
Old historicism still backing on into the future meets the New�though by now not so new�hermeneutically
front-loading the past, with not much more to show for the meeting in either direction than a bad case of
mutual dismissiveness.  The new textual materialities of the wired virtual present and future complicate these
too-easy divisions of cultural labor; the provisional methods philology favors to screen disparate but
synchronous cultural "documents" (for example, architectural remnants and texts) start to look a good deal
less peripheral to the principal business of interpretation, as we all have to be more or less good philologists
just to sort all that <EM>passes </EM>for evidence from one virtual source to the next.<A href="#N_7_"><SUP><font size=-2>7</font></SUP></A></P>

<P>	It is not very useful to dwell on semantic connections between theory and the stuff of philology if
philology and theory routinely gloss <EM>logos</EM> in two altogether distinct ways.  On the other hand, evidence-<EM>logos</EM> manifestly involves less source-hunting than source-criticism, and the mooted connection with theory
reminds us that early modern philology's most generative triumphs were virtually all triumphs of source-criticism, of exposing untrustworthy evidence as such, not just finding new documents or just noting new
sources and analogues.  Otherwise put, the poles of archival and critical reckoning with cultural documents
are already at home�and occasionally at odds�in traditional philology itself; if philology works by <EM>collatio
locorum</EM>, comparing analogous texts, it progresses by noting and disqualifying most forms of disjunction and
yet premising its own reconstructions on fragmented, discontinuous pasts.<A href="#N_8_"><SUP><font size=-2>8</font></SUP></A>  Philology can be conservative
and conservatorial or iconoclastic and disruptive, depending on which of these projects involves the greater
cultural urgency; in spite of its name it is not necessarily logocentric, since the work of disjunction treats
texts as both subject and scene of decentering.  If a rift of this sort is regarded as happening through time,
a disjunctive philology works to authorize one temporal stratum while slighting the rest; if the rift is regarded
as something intrinsic, a disjunctive, refractory philology will now work instead to call such authorized
schemes into question.<A href="#N_9_"><SUP><font size=-2>9</font></SUP></A>  This brings us back to future philologies, and to what provoked my choice of title;
it was precisely this sort of refractory philology, just this sort of refusal to swear by the premise that ancient
Greek culture cohered as a beautiful whole, that brought Nietzsche afoul of famed classicist Willamowitz-Moellendorff, who did all that he could to trash Nietzsche's <EM>Birth of Tragedy</EM> as a three-decker case of
perverse innovation, of preposterous "future-philology."<A href="#N_10_"><SUP><font size=-2>10</font></SUP></A>  This refractory, non-standardized handling of a
cultural archive is not unlike Bakhtin's way of wielding the tools of philology; it is arguably also the way of
a modernist poet like Eliot, and seems apt to be equally enlightening and equally enabling as a practice of
bona fide "future philology" and not just the joke-version sarcastically hailed by Nietzsche's critics.</P>

<P>	Backing up one more time to Bernard Knox's book, I should mention again that the talk on philology from that volume
I previously cited also boasts its own title: "On Two Fronts."  This refers to a need to contend with the rigors
of relevance (the future) as well as correctness (the past), with the classicist sadly not just backing into the
future but caught in the middle in the bargain.  Under this two-front siege it is somewhat surprising that two-fronted Janus does not enter into this picture, especially since two-fronted Janus is so handy and standard
a figure in Renaissance contexts for embodying philology's promise of personal and cultural empowerment
through retrograde progress, that "large discourse / Looking before and after" which lends Hamlet at one
point his best hope for timely initiative.<A href="#N_11_"><SUP><font size=-2>11</font></SUP></A>  The two faces of Janus might as well signify the conservative-innovative agendas of Renaissance philology; Arthur Koestler's example suggests they retain their attraction
as twin icons for cultural revisioning.<A href="#N_12_"><SUP><font size=-2>12</font></SUP></A></P>

<P>	<a name="Archimedes">It would probably be fair to describe the large interests of philology especially in the early modern
era as comprising not just evidence-studies but a larger mediation-critique; this would help to explain how
and why this particular discourse acquired such importance in an era marked by such extreme crises of
confidence in both temporal and spiritual mediators.<A href="#N_13_"><SUP><font size=-2>13</font></SUP></A>  It is in roughly this same connection, at a suitable historical distance, that I want to bring on the philologists' frolic invited by my visual aid for today ("<a href="pousto.htm">Virtual Standpoints: the Power of Outlandishness</a>"),
which could also germanely be titled "The Importance of Just Being Cranky," a thematic that suits Cowley's
work and large parts of our text-image archive in more ways than one.  We might also ask, "What is <i>not</i>
wrong with this picture?" but to find a good answer for that might take quite a long time; instead let's look at how
what's wrong <EM>works </EM>in this instance, how it reckons with compromised mediators.<A href="#N_14_"><SUP><font size=-2>14</font></SUP></A>  A main premise of the
volume where this emblem initially appears is that all Being alike is fair symbolic game for the self-celebration of the Jesuit order, coming up on its own hundredth birthday; in this instance an ancient scientific
conceit�Archimedes' supposed promise, "Give me a long enough lever and a place to stand and I will move
the world"�is turned into a jolly reflection (less benignly inflected, "Give some movers an inch and watch
them take a mile") on how the Jesuit order more or less out of nowhere has thrived so in its operations as to
come close to making the world go around.  Archimedes' long lever is turned into a luxury clockwork, but
a clockwork that shouldn't work even as well as the putative lever in a setup that furthermore seems to have
muddled designs on the globe at the end of the string (is it bent on sustaining its motion or reeling it in?).</P>

<P>	It makes slightly more sense of the image once we learn that Archimedes was also the maker of a
fabulously intricate clockwork-model of the cosmos, so that the emblem reunites different parts of his legend
at least;<A href="#N_15_"><SUP><font size=-2>15</font></SUP></A> but what are we to do with the floating machinery here that makes play with its own
insubstantialness,<A href="#N_16_"><SUP><font size=-2>16</font></SUP></A> a lot shakier than any Jacob's ladder that I can imagine, or of that beefy ephebe on the
right who looks more like a turnspit than angel?  It is not only that God has helped Jesuits make much out
of little from marginal points of departure the same way he helps England according to Marvell's and
Waller's poetic conceits;<A href="#N_17_"><SUP><font size=-2>17</font></SUP></A> the incongruous machinery of this emblem also seems to be only half-visible, so that secrecy plays
as important a part as disclosure in running the show.  The emblem tries to map Jesuit success as extending-mediating the spirit's intervention in matter, using here much the same sort of virtual nexus that Descartes
in about the same era deploys to give soul one small opening to work on his bodily machine;<A href="#N_18_"><SUP><font size=-2>18</font></SUP></A> but unlike Descartes' model, but remarkably like one of the protestant "self-consuming artifacts" of
Stanley Fish's study,<A href="#N_19_"><SUP><font size=-2>19</font></SUP></A> here the emblem seems cheerfully dead-set on flouting its own frame-conceit.  In this instance the
trick is the thing; God will work as he will; it makes Jesuits' success all the sweeter that all their success is
so painfully open and clear, incomplete as it is, while God's motives and methods are still partly mystifying. 
Otherwise put, this emblem is either a crass travesty of a heaven-and-earth mediational nexus�the irreverent
crowd might well read it in that way; so what?�or a harmless disposable clue as to what to and not to expect
from the Jesuit order; leave rapture to heaven, and trust all of the rest of Time's dance to the Jesuits.  Here I
think it would falsify matters <EM>not</EM> to read this and many such cruxes via future philology's refractory optics
or to argue away the discrepant and troublesome aspects of this emblematic amalgam; the text-image
renounces control in one cognitive frame to monopolize power in another: second-best, says the<EM> auctor pro
tem</EM>, but the best we can do.<br>
<br><CENTER>Notes </CENTER>
</P>


<P><A NAME="N_1_">1. </A>Descriptions courtesy of organizer Michael Levenson ("<a href="http://bodoni.village.virginia.edu:2020/xalan/servlet/DefaultApplyXSL/futures/futures.xml?xslURL=/futures/xsl/dynaxml.xsl">The Future of Literary Studies</a>," April 5-6, 2002).
<P><A NAME="N_2_">2. </A><EM>Backing into the Future: the Classical Tradition and its Renewal</EM> (New York, 1994). 301.
<P><A NAME="N_3_">3. </A><EM>On Philology</EM>, ed. Jan Ziolkowski (University Park, 1990; orig. published as <EM>Comparative
Literature Studies 27/1</EM>), 6; note the different inflection of this liminal take on the work of philology in Paul De Man's "The Return to Philology" (<i>The Resistance to Theory</i> [Minneapolis: University of Minnesota Press, 1986], 21-26).
<P><A NAME="N_4_">4. </A><EM>Hae nugae seria duc[u]nt</EM> (<EM>Moriae Encomium / Praise of Folly,</EM> Dedication to More, an arch
echo of Horace, <EM>A. P.</EM> 451).  For a wide range of similar claims in defense of Erasmian philology
see my "Erasmus' <EM>Adagia</EM>: Midwife to the Rebirth of Learning," <EM>JMRS</EM> 11(1981), esp. 173-74.
<P><A NAME="N_5_">5. </A>Leo Braudy, <EM>The Frenzy of Renown: Fame and its History</EM> (New York, 1986), 250, 258;
Augustine, <EM>De doctrina christiana</EM> 2.13.20 (a dogmatic new spin on a much older quarrel
between <EM>logos</EM> and <EM>logos</EM>, <EM>ratio</EM> and <EM>oratio</EM>, philosophy and rhetoric), discussed in my
introduction to <EM>In Defense of Humanism</EM>, <EM>The Complete Works of St. Thomas More</EM>, vol. 15 (New
Haven, 1986), xlviii-xlx.  With lvii n., citing Bacon's critique of unanchored Scholastic "webs of
learning" (<EM>Advancement of Learning</EM> 1.4.5 [<a href="http://darkwing.oregon.edu/~rbear/adv1.html">http://darkwing.oregon.edu/~rbear/adv1.html</a>]), cf.
12/19-21 n., on More's  contrary model of expansive, well-grounded philology.
<P><A NAME="N_6_">6. </A><EM>SP 98/2</EM> (2001) includes six well-done, <EM>au courant</EM> critical essays, with no trace of source-hunting or lexicographic philology; for a similar new direction in medieval studies,  see the
special "New Philology" issue of <EM>Speculum</EM> (<EM>65/1</EM> [1990]).
<P><A NAME="N_7_">7. </A>On philologist John Selden's "synchronism" criterion (connection and sorting of evidence by
temporal context avoiding anachronism) see D. R. Woolf, <EM>The Idea of History in Early Stuart
England</EM> (Toronto, 1990), esp. 212-13; on the genealogical method of establishing an authorized
text (to be sure, more appropriate for some cultural projects than others) see D. C. Greetham et
al.,<EM> Scholarly Editing: A Guide to Research</EM> (New York, 1995), <EM>s. v.</EM>  Jack Lynch argues that a
similar sort of sifting must be matter of course every time we resort to the virtual authorities of
the internet ("The Web of Disorderly Erudition: Electrifying the Eighteenth-Century Classroom"
[<a href="http://newark.rutgers.edu/~jlynch/Papers/web.html">http://newark.rutgers.edu/~jlynch/Papers/web.html</a>]).
<P><A NAME="N_8_">8. </A>With texts cited above, one might note the "principle of disjunction" famously formulated by
Erwin Panofsky to explain how the Renaissance constitutes itself as distinct from antiquity and
the Middle Ages (itself by the way a Renaissance construct); see his <EM>Renaissance and
Renascences in Western Art</EM> (New York, 1969), with discussion in my "Heirs of the Dog: Cynic
Selfhood in Medieval and Renaissance Culture," in <EM>The Cynic Movement in Antiquity and its
Legacy</EM>, ed. R. B. Branham and M..-O. Goulet-Caz&eacute; (Berkeley, 1996), esp. 305-06.  This new case-by-case sense for epochal disjunction is something quite different from the sense of disjunction-as-usual often marked in medieval unworldiness; see Charles Dahlberg, <i>The Literature of Unlikeness</i> (Hanover, NH, 1988), working out from Platonic accounts in Augustine's <i>Confessions</i> 7.10.16 and elsewhere of this world as a "region of unlikeness."
<P><A NAME="N_9_">9. </A>With David Hayman and Sam Slote, eds., <EM>Probes: Genetic Studies in Joyce</EM> (Amsterdam,
1995), and Bernard Cerquiglini, <EM>In Praise of the Variant: A Critical History of Philology</EM>, tr.
Betsy Wing (Baltimore, 1999), cf. Jerome McGann, <EM>The Textual Condition</EM> (Princeton, 1991).
<P><A NAME="N_10_">10. </A>Well if briefly discussed in James I. Porter's <EM>Nietzsche and the Philology of the Future</EM>
(Stanford, 2000), esp. 226-27; Porter goes on to sketch other aspects of Nietzsche's refractory
philology.  Willamowitz-Moellendorff's own text is translated and annotated in <i>New Nietzsche Studies 4/1</i> (2000), 1-32.  For Eliot's styles of refractory philology see espcially "<a href="http://www.english.uiuc.edu/maps/poets/a_f/eliot/tradition.htm">Tradition and the Individual Talent</a>" (1920) and <a href="http://people.a2000.nl/avanarum/"<i>The Waste Land</i></a> (1922), from the U. Illinois modern poetry site and the Eliot Hypertext Project. 
<P><A NAME="N_11_">11. </A><EM>Hamlet</EM> 4.4.36-37.  Hamlet's version of deliberate speed ("hastening slowly") may well seem
more deliberate than speedy, as he complains here; it contrasts with the unseemly haste of his
mother according to 1.2.152-53, as if she were a "beast that wants discourse of reason."  On
philology's regimen of retrograde progress see <EM>CW 15</EM>, lxviii, lxxvii, Erasmus' adage essay
<EM>Festina Lente</EM> ("Hasten Slowly," now online at the Philological Museum [<a href="http://e3.uci.edu/~papyri/speude">http://e3.uci.edu/~papyri/speude</a>]), and Timothy
Hampton's introduction to <EM>Writing from History: the Rhetoric of Exemplarity in Renaissance
Literature</EM> (Ithaca, 1990), along with Machiavelli's directions (<EM>The Prince</EM>, chap. 24) on how "to
make the new seem old."
<P><A NAME="N_12_">12. </A><EM>Janus: a Summing Up</EM> (New York, 1978).
<P><A NAME="N_13_">13. </A>Two especially drastic expressions of this double crisis, also usefully treated as a crisis of
ceremonial mediation, are the protestant Reformation and the Puritan revolution in England; with
Douglas F. Rutledge, ed., <EM>Ceremony and Text in the Renaissance</EM> (Newark, DE, 1996), and
Achsah Guibbory, <EM>Ceremony and Community from Herbert to Milton: Literature, Religion, and
Cultural Conflict in Seventeenth-Century England</EM> (Cambridge, 1998), see C. Condren, <i>The Language of Politics in Seventeenth-Century England</i> (New York, 1994), 155-58, on reform (cutting back the to the basics) as a nursery for Root-and-Branch radicalism.
<P><A NAME="N_14_">14. </A>For a similar mixed message in a Commonwealth emblem of Cromwell see "The Embleme of
ENGLANDS Distractions" with discussion by Bruce Lawson, "The Body as Political Construct"
(link and fuller reference at <a href="../cromwell.html#links">http://cowley.lib.virginia.edu/cromwell.html#links</a>).
<P><A NAME="N_15_">15. </A>See now <a href="http://www.mcs.drexel.edu/~crorres/Archimedes/Sphere/SphereSources.html">http://www.mcs.drexel.edu/~crorres/Archimedes/Sphere/SphereSources.html</a> for the
following ancient sources in translation: Cicero, <i>De rep.</i> 1.21-22; <i>Disp. Tusc.</i> 1.25.63; Ovid, <i>Fasti</i> 6.277-79; Lactantius, <i>Div. inst.</i> 2.5; Claudian, <i>De sphaera Archim.</i>; Proclus, <i>Comm. in lib. I Elem. Euclidis</i> 1.13; for his putative lever-and-standpoint proposal see Pliny, <i>NH</i> 7.37.  For a particularly beautiful modern (1998) version of this planetarium or orrery see <a href="http://www.clockmaking.co.uk/gallery_orrery6.jpg">http://www.clockmaking.co.uk/gallery_orrery6.jpg</a>.
<P><A NAME="N_16_">16. </A>A few floating devices are common in emblems, but this one goes well past the normative;
see B.F. Scholz, "'Ownerless Legs or Arms Stretching from the Sky': Notes on an Emblematic
Motif," in <EM>Andrea Alciato and the Emblem Tradition: Essays in Honor of Virginia Woods
Callahan</EM> (New York, 1989), 249-283.
<P><A NAME="N_17_">17. </A>See Andrew Marvell, "The First Anniversary [1654] of the Government under O[liver]
C[romwell]" 99-100, and Edmund Waller, "To the King [Charles II], upon his Majesty's Happy
Return" 51-54, both passages linked via <a href="../small/pousto.htm">http://cowley.lib.virginia.edu/small/pousto.htm</a>.
<P><A NAME="N_18_">18. </A>Descartes' virtual opening for spirit is the pineal gland under the brain; he completes his
account of this theory in <EM>The Passions of the Soul</EM> (1646, pub. 1649).  See the summary in
Stephen M. Fallon, <EM>Milton among the Philosophers: Poetry and Materialism in Seventeenth-Century England</EM> (Ithaca, 1991), 26-27.
<P><A NAME="N_19_">19. </A><EM>Self-Consuming Artifacts: The Experience of Seventeenth-Century Literature</EM> (Berkeley,
1972).  For the "dance to the music of Time" which helps offset this picture see <a href="vertumnu.htm#Vertumnus">http://cowley.lib.virginia.edu/small/vertumnu.htm#Vertumnus</a>.
</BODY>
</HTML>
