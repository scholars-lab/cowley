# Cowley project archive
Static generated files from http://cowley.lib.virginia.edu

## Process of creating the static archive
- Scraping the old site was done with this command:
  - `wget --mirror -P static-content -nH -np -p -k -E http://cowley.lib.virginia.edu/`
- All files were archived in a bzip2.tar file using this command:
  - `tar -jcf cowley.bzip2.tar /usr/local/projects/cowley/releases/20120820161240`
- To recreate the site
  - Put the contents of the 'static-content' folder on a web server. It is just static HTML files.
  - Or use Docker to build an nginx container with the folder 'static-content' linked to the folder /usr/share/nginx/html in the container.
    - The docker-compose.yml file needs to have the static-content folder and an nginx default.conf file on the same level as itself.
    - The docker-compose.yml file requires Traefik to be running and a 'thenetwork' docker network to be created. See here: https://github.com/scholarslab/traefik

## File Structure
```
.
├── README.md
├── cowley.bzip2.tar
├── docker-compose.yml
└── static-content      = an rsync from the old server (gets all files even if
                          not linked on the site)
└── old-static          = original copy of the files, using the wget 

```

